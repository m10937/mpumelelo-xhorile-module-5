import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddFilesList extends StatefulWidget {
  const AddFilesList({Key? key}) : super(key: key);

  @override
  State<AddFilesList> createState() => _AddFilesListState();
}

class _AddFilesListState extends State<AddFilesList> {
  final Stream<QuerySnapshot> _myInformation =
      FirebaseFirestore.instance.collection('information').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _myInformation,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.width),
                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data()! as Map<String, dynamic>;
                      return ListTile(
                        leading: Text(data['Person_Name']),
                        title: Text(data['Person_Surname']),
                        subtitle: Text(data['location']),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ],
          );
        } else {
          return (const Text('No data'));
        }
      },
    );
  }
}
