import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'list.dart';

class AddFiles extends StatefulWidget {
  const AddFiles({Key? key}) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  State<AddFiles> createState() => _AddFilesState();
}

// ignore: unused_element
class _AddFilesState extends State<AddFiles> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addFilesState() {
      final name = nameController.text;
      final surname = surnameController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection('information').doc();

      return ref
          .set({
            'Person_Name': name,
            'Person_Surname': surname,
            'location': location,
            'doc_id': ref.id
          })
          .then((value) => log('Info added!!'))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((120)),
                  ),
                  hintText: 'Enter Name',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: surnameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: 'Enter Surname',
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular((20)),
                  ),
                  hintText: 'Enter Location',
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                _addFilesState();
              },
              child: const Text('Add'),
            ),
            const SizedBox(
              height: 24,
            ),
            ElevatedButton(
              onPressed: () {
                final docUser = FirebaseFirestore.instance
                    .collection('information')
                    .doc('my-id');
                _addFilesState();

                docUser.update({});
              },
              child: const Text("Update"),
            ),
            const SizedBox(
              height: 24,
            ),
            ElevatedButton(
              onPressed: () {
                final docUser = FirebaseFirestore.instance
                    .collection('information')
                    .doc('my-id');
                _addFilesState();

                docUser.delete();
              },
              child: const Text("Delete"),
            ),
            const SizedBox(
              height: 24,
            ),
          ],
        ),
        const AddFilesList()
      ],
    );
  }
}
